package raumschiff;

/**
 * Diese Klasse erzeugt eine Ladung.
 * 
 * @author P�tschke
 * @version 1.0
 */
public class Ladung {

	private String bezeichnung;
	private int menge;

	/**
	 * Parameterloser Standard-Konstrukter.
	 */
	public Ladung() {
	}

	/**
	 * Konstruiert eine Ladung mit �blichen Parametern.
	 * 
	 * @param bezeichnung
	 *            Name der Ladung
	 * @param menge
	 *            Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	// Name Ladung
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBezeichnung() {
		return this.bezeichnung;
	}

	// Menge Ladung
	public void setMenge(int menge) {
		this.menge = menge;
	}

	public int getMenge() {
		return this.menge;
	}
}