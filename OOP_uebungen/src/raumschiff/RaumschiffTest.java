package raumschiff;

/**
 * Die Klasse f�hrt das Programm aus.
 * 
 * @author P�tschke
 * @version 1.0
 */
public class RaumschiffTest {

	/**
	 * Anlegen der Raumschiff- und Ladungsobjekte mit �blichen Parametern. 
	 * Aufrufen der Methoden aus den Klassen Raumschiff und Ladung.
	 * 
	 * @param args
	 *            Die Parameter, den der Benutzer angibt, wenn er das Programm startet.
	 */
	public static void main(String[] args) {

		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni Var");

		Ladung klingonen1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonen2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung romulaner1 = new Ladung("Borg-Schrott", 5);
		Ladung romulaner2 = new Ladung("Rote Materie", 2);
		Ladung romulaner3 = new Ladung("Plasma-Waffe", 50);
		Ladung vulkanier1 = new Ladung("Forschungssonde", 35);
		Ladung vulkanier2 = new Ladung("Photonentorpedos", 3);

		klingonen.addLadung(klingonen1);
		klingonen.addLadung(klingonen2);

		romulaner.addLadung(romulaner1);
		romulaner.addLadung(romulaner2);
		romulaner.addLadung(romulaner3);

		vulkanier.addLadung(vulkanier1);
		vulkanier.addLadung(vulkanier2);

		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);

		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch!");

		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();

		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();

		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);

		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();

		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();

		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
	}
}
