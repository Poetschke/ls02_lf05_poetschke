package raumschiff;

import java.util.ArrayList;

/**
 * Diese Klasse erzeugt ein Raumschiff.
 * 
 * @author P�tschke
 * @version 1.0
 *
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Parameterloser Standard-Konstrukter.
	 */
	public Raumschiff() {
	}

	/**
	 * Konstruiert ein Raumschiff mit �blichen Zust�nden.
	 * 
	 * @param photonentorpedoAnzahl
	 *            Menge an Photonentorpedos
	 * @param energieversorgungInProzent
	 *            Zustand der Energieversorgung
	 * @param schildeInProzent
	 *            Zustand der Schutzschilde
	 * @param huelleInProzent
	 *            Zustand der Schiffsh�lle
	 * @param lebenserhaltungssystemeInProzent
	 *            Zustand der Lebenserhaltungssysteme
	 * @param androidenAnzahl
	 *            Menge an Androiden
	 * @param schiffsname
	 *            Name des Raumschiffes {@code String}
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	// Anzahl Photonentorpedos eines Raumschiffes
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	// Energieversorgung eines Raumschiffes
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	// Schutzschilde eines Raumschiffes
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	// Huellenstabilit�t eines Raumschiffes
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	// Lebenserhaltungssysteme eines Raumschiffes
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	// Anzahl an Androiden eines Raumschiffes
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	// Name eines Raumschiffes
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	// Ladung zum Raumschiff hinzuf�gen
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Falls mind. 1 Photonentorpedo auf dem Raumschiff vorhanden ist, wird ein Photonentorpedo von 
	 * diesem auf ein weiteres Raumschiff abgeschossen, eine Nachricht an alle Raumschiffe gesendet 
	 * und die Methode treffer(r) aufgerufen. Andernfalls geschieht nichts.
	 * 
	 * @param r
	 *            Raumschiff, auf das geschossen wird
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen!");
			treffer(r);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * Falls genug Energie auf einem Raumschiff vorhanden ist, wird eine Phaserkanone von diesem 
	 * auf ein weiteres Raumschiff abgeschossen, eine Nachricht an alle Raumschiffe gesendet und 
	 * die Methode treffer(r) aufgerufen. Andernfalls geschieht nichts.
	 * 
	 * @param r
	 *            Raumschiff, auf das geschossen wird
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent > 49) {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			treffer(r);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * Das Ausma� eines Treffers wird berechnet. Auf der Konsole wird ausgegeben,
	 * dass ein Raumschiff getroffen wurde. Falls der Zustand der Schiffs�lle auf 0
	 * sinkt, wird eine Nachricht an alle Raumschiffe gesendet.
	 * 
	 * @param r
	 *            Raumschiff, auf das geschossen wird
	 */
	private void treffer(Raumschiff r) {
		System.out.println("\n" + r.schiffsname + " wurde getroffen!");
		r.schildeInProzent = r.schildeInProzent - 50;
		if (r.schildeInProzent <= 0) {
			r.schildeInProzent = 0; // Dient dazu, um zu verhindern, dass ein Negativer Wert angezeigt wird
			r.huelleInProzent = r.huelleInProzent - 50;
			r.energieversorgungInProzent = r.energieversorgungInProzent - 50;
		}
		if (r.energieversorgungInProzent < 0) {
			r.energieversorgungInProzent = 0;
		}
		if (r.huelleInProzent <= 0) {
			r.huelleInProzent = 0;
			r.lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle("Lebenserhaltungssysteme wurden vernichtet!");
		}
	}

	/**
	 * Mit dieser Methode werden Nachrichten an alle gesendet. Die Nachrichten werden dem 
	 * BroadcastKommunikator hinzu gef�gt. Auf der Konsole wird diese Nachricht ausgegeben.
	 * 
	 * @param message
	 *            Der Text {@code String}, der als Nachricht �bergeben und gesendet werdern soll.
	 */
	public void nachrichtAnAlle(String message) {
		// Die Nachricht wird dem broadcastKommunikator hinzugef�gt
		broadcastKommunikator.add(message);
		System.out.println("\nNachricht an Alle: " + message);
	}

	/**
	 * In dieser Methode werden die Logbucheintr�ge in einer Liste in der Kosnole
	 * ausgegeben. Falls keine Nachrichten im BroadcastKommunikator vorhanden sind,
	 * wird eine Info in der Konsole ausgegeben.
	 * 
	 * @return broadcastKommunikator Gibt die Eintr�ge des broadcastKommunikator zur�ck.
	 */
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		System.out.println("\nLogbuch:");
		if (broadcastKommunikator.isEmpty()) {
			System.out.println("Keine Eintr�ge vorhanden!");
		} else {
			System.out.println(broadcastKommunikator);
		}
		return broadcastKommunikator;
	}

	/**
	 * Pr�ft die Anzahl an Photonentorpedos aus der Ladung des Raumschiffes und ladet 
	 * diese in das Raumschiff. Je nach �bergebener Anzahl wird die Ladung neuberechnet 
	 * und die urspr�ngliche Anzahl ge�ndert.
	 * 
	 * @param anzahlTorpedos
	 *            Anzahl der zu ladenden Photonentorpedos
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		int ladungTorpedos;
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			// Abfrage der Menge an Photonentorpedos aus der Ladung des Raumschiffes
			if (ladungsverzeichnis.get(i).getBezeichnung() == "Photonentorpedos") {
				ladungTorpedos = ladungsverzeichnis.get(i).getMenge();
				if (ladungTorpedos == 0) {
					broadcastKommunikator.add("-=*Click*=-");
					System.out.println("Keine Photonentorpedos gefunden!");
				} else {
					if (anzahlTorpedos > ladungTorpedos) {
						setPhotonentorpedoAnzahl(photonentorpedoAnzahl + ladungTorpedos);
						ladungsverzeichnis.get(i).setMenge(0);
						System.out.println(schiffsname + " hat nicht gen�gend Photonentorpedos, es werden "
								+ ladungTorpedos + " Photonentorpedos eingesetzt.");
					} else {
						setPhotonentorpedoAnzahl(photonentorpedoAnzahl + anzahlTorpedos);
						ladungsverzeichnis.get(i).setMenge(ladungTorpedos - anzahlTorpedos);
						System.out.println("\n" + anzahlTorpedos + " Photonentorpedo(s) eingesetzt!");
					}
				}
			}
		}
	}

	/**
	 * Neuberechnung der Zust�nde, durch Pr�fung und Z�hlung der auf "true" gesetzten Paramater,
	 * der Anzahl vorhandener und �bergebener Androiden. Die Zust�nde werden neu und zu�llig 
	 * berechnet. Die urspr�nglichen Zustandswerte werden mit den neuen Werten ge�ndert.
	 * 
	 * @param schutzschilde
	 *            Wahrheitswert {@code boolean}
	 * @param energieversorgung
	 *            Wahrheitswert {@code boolean}
	 * @param schiffshuelle
	 *            Wahrheitswert {@code boolean}
	 * @param anzahlEinzusetzendeDroiden
	 *            Anzahl der zu verwendenden Androiden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlEinzusetzendeDroiden) {

		// auf true gesetzte Parameter z�hlen f�r die sp�ter folgende Berechnung
		int Parameter = 0;

		if (schutzschilde == true) {
			Parameter += 1;
		}
		if (energieversorgung == true) {
			Parameter += 1;
		}
		if (schiffshuelle == true) {
			Parameter += 1;
		}
		// Anweisung um Compilerfehler auszuschlie�en, denn 0 ist nicht teilbar
		if (Parameter == 0) {
			System.out.println("\nEs muss mindestens 1 Parameter auf true gesetzt werden!");
		} else {
			
			// Formel zur Berechnung der neuen Zustandswerte
			int zufallszahl = (int) (Math.random() * 100);
			int ergebnisProzent = (zufallszahl * anzahlEinzusetzendeDroiden) / Parameter;

			// geforderte Anzahl Androiden mit vorhandener ANzahl pr�fen
			if (androidenAnzahl == 0) {
				ergebnisProzent = 0;
				System.out.println(
						"\nEs sind keine Androiden auf dem Raumschiff und somit ist keine Reperaturen m�glich!");
			}
			if (anzahlEinzusetzendeDroiden > androidenAnzahl) {
				anzahlEinzusetzendeDroiden = androidenAnzahl;
				System.out.println("\nEs sind nicht gen�gend Androiden auf dem Raumschiff vorhanden!");
				System.out.println(anzahlEinzusetzendeDroiden
						+ " vorhandene Androiden wurden eingesetzt und konnten den Zustand um " + ergebnisProzent
						+ " % erh�hen!");
			}
			if (anzahlEinzusetzendeDroiden < androidenAnzahl) {
				System.out.println("\nDie eingesetzten " + anzahlEinzusetzendeDroiden
						+ " Androiden konnten den Zustand um " + ergebnisProzent + " % erh�hen!");
			}

			// alte Werte mit neu berechneten Werten �ndern
			if (schutzschilde == true) {
				setSchildeInProzent(schildeInProzent + ergebnisProzent);
				// System.out.println("Die Schutzschilde wurden auf " + getSchildeInProzent() +
				// " % erh�ht.");
			}
			if (energieversorgung == true) {
				setEnergieversorgungInProzent(energieversorgungInProzent + ergebnisProzent);
				// System.out.println("Die Energieversorgung wurde auf " +
				// getEnergieversorgungInProzent() + " % erh�ht.");
			}
			if (schiffshuelle == true) {
				setHuelleInProzent(huelleInProzent + ergebnisProzent);
				// System.out.println("Die Schiffsh�lle wurde auf " + getHuelleInProzent() + " %
				// erh�ht.");
			}
		}
	}

	/**
	 * Gibt die Zust�nde eines Raumschiffes in der Konsole aus.
	 */
	public void zustandRaumschiff() {
		System.out.println("\nDas Raumschiff " + schiffsname + " hat folgende Zust�nde:");
		System.out.println("Schiffsame: " + schiffsname);
		System.out.println("Anzahl Photonentorpedo: " + photonentorpedoAnzahl);
		System.out.println("Energie: " + energieversorgungInProzent + " %");
		System.out.println("Schilde: " + schildeInProzent + " %");
		System.out.println("Huelle: " + huelleInProzent + " %");
		System.out.println("Lebenserhaltungsysteme: " + lebenserhaltungssystemeInProzent + " %");
		System.out.println("Anzahl Androiden: " + androidenAnzahl);
	}

	/**
	 * Gibt die Ladung eines Raumschiffes in der Konsole aus. Ist keine Ladung
	 * vorhanden, wird dies in der Konsole ausgegeben.
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nDas Raumschiff " + schiffsname + " hat folgende Ladung:");
		if (ladungsverzeichnis.isEmpty()) {
			System.out.println("Keine Ladung vorhanden!");
		} else {
			for (int i = 0; i < ladungsverzeichnis.size(); i++) {
				System.out.println("Bezeichnung: " + ladungsverzeichnis.get(i).getBezeichnung() + " Menge: "
						+ ladungsverzeichnis.get(i).getMenge());
			}
		}
	}

	/**
	 * Eine Ladung mit der Menge 0 wird aus dem Verzeichnis entfernt.
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = ladungsverzeichnis.size(); --i >= 0;) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(ladungsverzeichnis.get(i));
			}
		}
	}
}